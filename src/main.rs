use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use std::process::Command;
use serde_json::{json, Value};

async fn get_sentiment(text: web::Json<Value>) -> impl Responder {
    // Call the python script with the text as an argument
    let input_value = text.into_inner();
    let input_text = input_value.get("text").unwrap().as_str().unwrap();
    let output = Command::new("python3")
        // .arg("src/model_helper.py")
        .arg("/usr/local/bin/model_helper.py")
        .arg(input_text)
        .output();

    // Check if the command was successful
    match output {
        Ok(output) => {
            // Parse the output as a string
            let output_str = String::from_utf8(output.stdout).unwrap();
            let output_json: Value = serde_json::from_str(&output_str).unwrap();
            HttpResponse::Ok().json(output_json)
        }
        Err(err) => {
            // Return an error response if the command failed
            HttpResponse::InternalServerError().json(json!({
                "error": err.to_string()
            }))
    }
}
}
        
    
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/sentiment", web::post().to(get_sentiment))
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}

// curl -X POST -H "Content-Type: application/json" -d '{"text": "I love rust, how about you?"}' http://localhost:8080/sentiment