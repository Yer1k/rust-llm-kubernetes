import json
import sys
from transformers import pipeline



def get_sentiment_analysis(text):
    """
    Get sentiment analysis of text
    Input: text (str)
    Output: json object with sentiment analysis results
    """
    sentiment_analysis = pipeline(model="distilbert-base-uncased-finetuned-sst-2-english")
    return sentiment_analysis(text)

if __name__ == "__main__":
    try:
        text = sys.argv[1]
        inference = get_sentiment_analysis(text)
        print(json.dumps({"Sentiment Analysis": inference}))
    except Exception as e:
        print(json.dumps({"error": str(e)}))