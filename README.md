# Rust LLM Kubernetes

[![CI/CD status](https://gitlab.com/dukeaiml/IDS721/rust-llm-kubernetes/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/rust-llm-kubernetes/-/commits/main)

Group #7: [Dingkun Yang](https:yer1k.com), [Sukhpreet Sahota](https://gitlab.com/5ukhy21), [Suzanna Thompson](https://gitlab.com/srt32)

This project contains a dockerized sentiment analysis application that uses Open LLM on Hugging Face with Rust Actix Web Services and Kubernetes. 

This application uses an open-source model, [distilbert/distilbert-base-uncased-finetuned-sst-2-english](https://huggingface.co/distilbert/distilbert-base-uncased-finetuned-sst-2-english), from Hugging Face that allows users to prompt with a json payload that contains their desired text. The model returns the sentiment, either `NEGATIVE` or `POSITIVE` as `label`, and a confidence `score`, ranging from 0 to 1. This allows the end user to understand the sentiment of the text they input and then conduct further analysis based on the confidence score by setting a desired threshold based on their use cases.

## Application Flowchart

![Process](screenshots/Final Project Process Diagram.png)

## Demo
Please click on the image below to view the demo video.
[![Demo](screenshots/thumbnail.png)](https://youtu.be/thDY78DQ6Wo)

#### CI/CD Pipeline on GitLab to AWS

We use GitLab CI/CD to build and push the Docker image to AWS ECR and deploy the application to AWS EKS. With the pipeline and CI/CD configuration, i.e. variables as shown below, we can automate the deployment process.

![CI/CD Pipeline](screenshots/cicd_pipeline.png)
![CI/CD Variables](screenshots/cicd_variables.png)

#### AWS API Endpoint

```bash
curl -X POST -H "Content-Type: application/json" -d '{"text": "Wow, we love AWS Cloud Services"}' http://a5555caf8cc7242889bff784915d1bd9-1162981651.us-east-1.elb.amazonaws.com:8080/sentiment
```

### Azure API Endpoint

```bash
curl -X POST -H "Content-Type: application/json" -d '{"text": "I do think MIDS is great, but what do you think? Huh?"}' http://llmrustazure-dns-ihr99kcw.hcp.eastus.azmk8s.io:8080/sentiment 
```

#### Running Locally

Once started the application with `cargo run`, you can access the application at `http://localhost:8080`.

Using the following command, you can send a POST request to the application with a JSON payload containing the text you want to analyze.

```bash
curl -X POST -H "Content-Type: application/json" -d '{"text": "I love rust, how about you?"}' http://localhost:8080/sentiment
```

Then you should see the following response, as shown in the screenshot below:

```json
{"Sentiment Analysis":[{"label":"POSITIVE","score":0.9936991930007936}]}
```

![Running Locally](screenshots/local_run.png)

#### Running on Docker Container

You can also run the application in a Docker container.

First, build the Docker image using the following command:

```bash
docker build -t rust-llm-kubernetes .
```

![Building Docker Image](screenshots/docker_build.png)

Then run the Docker container using the following command:

```bash
docker run -d -p 8080:8080 rust-llm-kubernetes
```

![Docker image](screenshots/docker_image.png)

After it is started, you can access the application at `http://localhost:8080` and send a POST request to the application with a JSON payload containing the text you want to analyze.

```bash
curl -X POST -H "Content-Type: application/json" -d '{"text": "I think MIDS is great, but what do you think? Huh?"}' http://localhost:8080/sentiment
```

Then you should see the following response, as shown in the screenshot below:

```json
{"Sentiment Analysis":[{"label":"POSITIVE","score":0.8286094069480896}]}
```

![Running on Docker Container](screenshots/docker_run.png)


## Deploying on AWS ECR 

This project was pushed to AWS Elastic Container Registry in a public repository. 

This makes the container publically available.

![Pushing to AWS ECR](screenshots/aws_ecr_docker_push.png)

Below is an image of the public repository. 

![ECR Repo](screenshots/aws_ecr_public_repo.png)

Below is an image of the AWS Elastic Kubernetes Services and the associated working pods.

![EKS Clusters](screenshots/eks_cluster.png)

![EKS Clusters Pods](screenshots/eks_cluster_pods.png)

After creating the Kubernetes clusters, we needed to grant the appropriate permission policies, in particular the eks:DescribeCluster (that was enabled through a JSON inline upload, which is titled describe_cluster_policy in the image below).

![IAM Policy](screenshots/aws_iam_policies.png)

Below is an image of the AWS EC2 instances we created for this project as computing resources.

![EC2 Instances](screenshots/ec2_instances.png)

![EC2 Instances](screenshots/ec2_instances_2.png)

To ensure our computing resources were utilized efficiently, we also set up a load balancer.

![EC2 Load Balancer](screenshots/ec2_load_balancer.png)

To monitor our application/program, we set up CloudWatch alarms.

![CloudWatch Dashboard](screenshots/cloudwatch_dashboard.png)

![CloudWatch Alarms](screenshots/cloudwatch_alarms.png)

## Deployed to Azure

This project was also pushed to Azure Container Registry in a public repository.

![Azure ACR](screenshots/azure_kubernetes.png)

Below is an image of the repository.

![Azure CR](screenshots/azure_cr.png)

While the Azure Kubernetes Service is running, you can see the log as shown below.

![Azure Kubernetes](screenshots/azure_kubernetes.png)

### Azure Logging

![Azure Logging](screenshots/azure_log.png)