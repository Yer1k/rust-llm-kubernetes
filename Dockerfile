# Description: Dockerfile for building the rust-llm-kubernetes binary

# Use the rust image as the base image
FROM rust:1.72 as builder

WORKDIR /usr/src/rust-llm-kubernetes

COPY ./Cargo.toml ./Cargo.lock ./

RUN mkdir src/ && \
    echo "fn main() {println!(\"This is the warning that the build is unsuccesful. Better check that out.\")}" > src/main.rs && \
    cargo build --release && \
    rm -f target/release/deps/rust_llm_kubernetes*

COPY ./src ./src

RUN cargo build --release

# Setup the python image
From python:3.9-slim

WORKDIR /rust-llm-kubernetes

# Install the required packages
RUN apt-get update && apt-get install -y \
    libssl-dev \
    && rm -rf /var/lib/apt/lists/*

# Install Rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"

# Install PyTorch and transformers
RUN pip install --no-cache-dir torch transformers

# Copy the binary and the python script
COPY --from=builder /usr/src/rust-llm-kubernetes/target/release/rust-llm-kubernetes .
COPY ./src/model_helper.py /usr/local/bin/
RUN chmod +x /usr/local/bin/model_helper.py

# Set the entrypoint
ENTRYPOINT ["./rust-llm-kubernetes"]

# Expose the port
EXPOSE 8000

# Command to run the binary
CMD ["./rust-llm-kubernetes"]